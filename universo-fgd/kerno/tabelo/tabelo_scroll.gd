extends ScrollContainer


# нажата кнопка на поле
# index - какая строка выделена
# event - какая кнопка мыши нажата
signal Horizontalo(index, event)

@onready var control = $Control


var table = {
	'vertikalo':0,
	'horizontalo':0,
	'name_vertikalo':[],
	# ширина́ (размер) larĝo
	'largho':[],
	'horizontaloj':[],
	'identigo':[] # признак идентификации (identigo) каждой строки
}
var stylebox_normal
var stylebox_distingo  # (отличение) distingo
var distingo_horizontalo = 0 # выделенная строка
var _mouse_offset = 0 # смещение мышки после нажатия кнопки

#TODO: Refactor
#func _ready():
	#stylebox_distingo = $Control/Label3.get_stylebox("normal").duplicate()
	#stylebox_normal = $Control/Label2.get_stylebox("normal").duplicate()
	#plenigi_formularon()


func largho_vert(vert, largho):
	#блок проверки длины строки
	var st = vert
	if st.length()>(largho/7.142857143):
#		print('st = ',st.erase(largho/7.142857143,st.length()),' = ',st)
		st.erase(largho/7.142857143,st.length())
	return st


# очистка всей таблицы (вместе с заголовками)
func forigi_tabelo():
	for cnt in control.get_children():
		cnt.queue_free()


# очистка данных в таблице
func forigi_horizontalo():
	table['horizontalo'] = 0
	table['horizontaloj'] = []
	


#plenigi formularon - заполнить форму
func plenigi_formularon():
	forigi_tabelo()
	var lbl
	var position_horizontalo = 0 # какая строка
	var position_vertikalo = 0 # начало колонки
	for vertikalo in table['vertikalo']:
		var largho = table['largho'][vertikalo]
		lbl = Label.new()
		lbl.name = 'name_vertikalo'+String(vertikalo+1)
		lbl.text = table['name_vertikalo'][vertikalo]
		lbl.text = largho_vert(lbl.text, largho)
		lbl.offset_left = position_vertikalo + 3
		lbl.offset_top = 0
		control.add_child(lbl,true)
		
		position_horizontalo = 2
		for horizontalo in table['horizontalo']:
			lbl = Label.new()
			lbl.name = String(vertikalo+1) + '_vertikalo_' + String(horizontalo+1) + '_horizontalo_'
			lbl.text = table['horizontaloj'][horizontalo][vertikalo]
			lbl.offset_left = position_vertikalo + 3
			lbl.offset_top = position_horizontalo * 16
			#блок проверки длины строки
			lbl.text = largho_vert(lbl.text, largho)
			lbl.offset_right = lbl.offset_left + table['largho'][vertikalo]
			if distingo_horizontalo == horizontalo:
	#			new_stylebox_normal.border_width_top = 3
	#			new_stylebox_normal.border_color = Color(0, 1, 0.5)
				lbl.add_theme_stylebox_override("normal", stylebox_distingo)
			else:
				lbl.add_theme_stylebox_override("normal", stylebox_normal)
			lbl.mouse_filter = MOUSE_FILTER_STOP
			lbl.connect("gui_input", Callable(self, "_on_Label_gui_input").bind(vertikalo, horizontalo))
			control.add_child(lbl, true)

			position_horizontalo += 1
		position_vertikalo += table['largho'][vertikalo]
		var rect = VSeparator.new()
		rect.offset_left = position_vertikalo
		rect.offset_top = 0
		rect.offset_bottom = (table['horizontalo'] + 2) * 16
		rect.name = 'rect' + String(vertikalo+1)
		rect.set_default_cursor_shape(Control.CURSOR_HSIZE)
		rect.connect("gui_input", Callable(self, "_on_ColorRect_gui_input").bind(vertikalo))
		control.add_child(rect, true)
	var rect = HSeparator.new()
	rect.offset_left = 0
	rect.offset_top = 18
	rect.offset_right = position_vertikalo
	rect.name = 'HSeparator'
	control.add_child(rect, true)
	control.custom_minimum_size.x = position_vertikalo


func _on_ColorRect_gui_input(event, vertikalo):
	# vertikalo -  какую разделительную двигали
	if event is InputEventMouseButton and event.button_index==1:
		if event.pressed:
			# запоминаем позицию мышки
			_mouse_offset = get_global_mouse_position().x
		else:
			# считываем позицию мышки и высчитываем разницу, далее всё сдвигаем
			_mouse_offset = get_global_mouse_position().x - _mouse_offset
			# изменяем размер столбца
			table['largho'][vertikalo] = table['largho'][vertikalo] + _mouse_offset
			plenigi_formularon()


func resize(what):
	var line = 0
	for lbl in control.get_children():
		if lbl.name.count('vertikalo'): 
			var st = lbl.text
			var largho_vertikalo = lbl.offset_right - lbl.offset_left
			if st.length()>(largho_vertikalo/7.142857143):
				st.erase(largho_vertikalo/7.142857143,st.length())
				lbl.text = st


func _on_Label_gui_input(event, vertikalo, horizontalo):
	if event is InputEventMouseButton and event.pressed:
		distingo(horizontalo, event)


# какая линия будет выделенной
func distingo(horizontalo, event):
	if distingo_horizontalo == horizontalo:
		return
	var line = 0
	for lbl in control.get_children():
		if lbl.name.count('_horizontalo_'): 
			var str1 = lbl.name
			str1.erase(0,lbl.name.find('_vertikalo_')+11)
			str1.erase(str1.find('_horizontalo_'),len(str1))# удалили окончание
			var hor = int(str1)-1
			if hor == distingo_horizontalo:
				lbl.add_theme_stylebox_override("normal", stylebox_normal)
			elif hor == horizontalo:
				lbl.add_theme_stylebox_override("normal", stylebox_distingo)
	distingo_horizontalo = horizontalo
	emit_signal("Horizontalo", distingo_horizontalo, event)


# добавить колонку
func aldoni_vertikalo(nomo_vertikalo, largho = 100):
	if !nomo_vertikalo:
		return
	table['vertikalo'] += 1
	table['name_vertikalo'].append(nomo_vertikalo)
	table['largho'].append(largho)
#	for horiz in table['horizontaloj']:
#		horiz.append('')


func aldoni_horizontalo(informoj, identigo = null):
	if !informoj:
		return
	table['horizontaloj'].append(informoj)
	table['identigo'].append(identigo)
	table['horizontalo'] += 1


# функция обновления конкретной строки (определяется по совпадению identigo)
# renovigi - обновить
func renovigi(informiloj, identigo):
	var index = 0
	for ident in table['identigo']:
		if ident == identigo:
			# обновляем соответствующую строку и выводим её на экран
			table['horizontaloj'][index] = informiloj
			renovigi_horizontalo(index)
			return
		index += 1
	# не обнаружено - добавляем новую строку
	aldoni_horizontalo(informiloj, identigo)
	plenigi_formularon()


# функция обновления на экране конкретной строки
# renovigi - обновить
func renovigi_horizontalo(horizontalo):

	for lbl in control.get_children():
		if lbl.name.count('_horizontalo_'): 
			var str1 = lbl.name
			str1.erase(0,lbl.name.find('_vertikalo_')+11)
			str1.erase(lbl.name.find('_horizontalo_'),len(str1))# удалили окончание
			var hor = int(str1)-1
			if hor == horizontalo:
				# обновить данную строку
				# выясняем какой столбец мы обновляем
				str1 = lbl.name
				str1.erase(lbl.name.find('_vertikalo_'),len(lbl.name))# удалили окончание
				if lbl.text != table['horizontaloj'][horizontalo][int(str1)-1]:
					lbl.text = table['horizontaloj'][horizontalo][int(str1)-1]


