extends Node2D


@onready var status = get_node("ParallaxBackground/Status")


func _ready():
	$'ParallaxBackground/GreetingLabel'.text = "Приветствую %s!" % Global.nickname
	if Net.connect("connection_failed", Callable(self, "_on_connection_failed")):
		print('error connect')
	if Net.connect("connection_succeeded", Callable(self, "_on_connection_success")):
		print('error connect')
	if Net.connect("server_disconnected", Callable(self, "_on_server_disconnect")):
		print('error connect')
	if Net.connected:
		status.text = "Connected"
		status.modulate = Color.GREEN
	else:
		status.text = "Server Disconnected, trying to connect..."
		status.modulate = Color.RED


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	status.text = "Connected"
	status.modulate = Color.GREEN


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	status.text = "Connection Failed, trying again"
	status.modulate = Color.RED


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	status.text = "Server Disconnected, trying to connect..."
	status.modulate = Color.RED


# обработчик сигнала прихода данных
func _on_data():
	pass

#===data= {id:1, payload:{data:{objekto:{edges:[{node:{koordinatoX:Null, koordinatoY:Null, koordinatoZ:Null, ligilo:{edges:[{node:{konektiloLigilo:Null, konektiloPosedanto:3, ligilo:{integreco:Null, ligilo:{edges:[...]}, nomo:{enhavo:Vostok Двигатель}, resurso:{objId:6}, uuid:79f80039-5ce3-4510-aeba-7278ba31ee0c}, tipo:{objId:1}}}, {node:{konektiloLigilo:Null, konektiloPosedanto:5, ligilo:{integreco:Null, ligilo:{edges:[...]}, nomo:{enhavo:Универсальный лазер}, resurso:{objId:11}, uuid:d89c7945-6486-4376-b673-8e31fc8363aa}, tipo:{objId:1}}}, {node:{konektiloLigilo:Null, konektiloPosedanto:1, ligilo:{integreco:Null, ligilo:{edges:[{node:{konektiloLigilo:6, konektiloPosedanto:5, ligilo:{integreco:Null, resurso:{objId:11}, uuid:d89c7945-6486-4376-b673-8e31fc8363aa}, tipo:{objId:2}}}, {node:{konektiloLigilo:4, konektiloPosedanto:3, ligilo:{integreco:Null, resurso:{objId:6}, uuid:79f80039-5ce3-4510-aeba-7278ba31ee0c}, tipo:{objId:2}}}, {node:{konektiloLigilo:Null, konektiloPosedanto:Null, ligilo:{integreco:Null, resurso:{objId:2}, uuid:d3f5f1bb-ce39-4a25-8e25-0372ec81c40e}, tipo:{objId:3}}}, {node:{konektiloLigilo:1, konektiloPosedanto:2, ligilo:{integreco:Null, resurso:{objId:5}, uuid:30d99d70-9207-4a25-8754-793cabef0b90}, tipo:{objId:2}}}]}, nomo:{enhavo:Vostok Модуль Кабины}, resurso:{objId:4}, uuid:fcbb8214-0ba2-426d-965c-09916a1750e1}, tipo:{objId:1}}}, {node:{konektiloLigilo:Null, konektiloPosedanto:2, ligilo:{integreco:Null, ligilo:{edges:[{node:{konektiloLigilo:3, konektiloPosedanto:4, ligilo:{integreco:Null, resurso:{objId:6}, uuid:769a94f2-ecde-4f2a-9124-dc6bbb27e907}, tipo:{objId:2}}}]}, nomo:{enhavo:Vostok Грузовой Модуль}, resurso:{objId:5}, uuid:30d99d70-9207-4a25-8754-793cabef0b90}, tipo:{objId:1}}}, {node:{konektiloLigilo:Null, konektiloPosedanto:4, ligilo:{integreco:Null, ligilo:{edges:[...]}, nomo:{enhavo:Vostok Двигатель}, resurso:{objId:6}, uuid:769a94f2-ecde-4f2a-9124-dc6bbb27e907}, tipo:{objId:1}}}]}, ligiloLigilo:{edges:[{node:{posedanto:{koordinatoX:50000, koordinatoY:0, koordinatoZ:0, k[...]
#[output overflow, print less text!]


# Обработчик сигнала "players_updated"
func update_players_list():
	pass
