extends "res://kerno/fenestroj/tipo_a1.gd"
# окно вывода лога среднего времени запросов

@onready var table = get_node("VBox/body_texture/table")


func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("tempo_informpetoj", Callable(self, "plenigi_formularon"))
	if err:
		print('error = ',err)
	table.columns = 3
	table.set_hide_root(true)
	# заполняем заголовки столбцов
	table.set_column_title(0,'имя запроса')
	table.set_column_title(1,'количество')
	table.set_column_title(2,'среднее время')
	table.set_column_titles_visible(true)



#plenigi formularon - заполнить форму
func plenigi_formularon():
	table.clear()
	table.create_item()
	var item
	for informpeto in Net.tempo_informpeto:
#		print('==informpeto== ',informpeto, ' == ',Net.tempo_informpeto.get(informpeto))
		var value = Net.tempo_informpeto.get(informpeto)
		item = table.create_item()
		item.set_text(0,informpeto)
		item.set_text(1, str(value['kvanto']))
		item.set_text(2, str(value['averagxo']))




func _on_table_focus_entered():
	fenestro_supren()

