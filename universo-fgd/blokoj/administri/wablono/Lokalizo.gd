extends Control

const DEFA_LINGO = 'ru_RU' # константа языка по умолнчанию, сейчас русский

var loka_info  # значения будут присвоены в функции analizo_json()
var loka_lingvoj = {} # списко языков с кодами из запросы списков языков, формат код:язык(оригинальное название)
var listo_de_lingvoj = [] #список названий языков для получения индекса
var listo_de_kodoj = [] #списко кодов языков
var chefa_varianto  #язык по умолчанию "Главный вариант", может быть только один
var nuna_lingvo = '' #current_lang - текущий язык (выбранный из списка)
var varia_valoro = '' # changed_value переменная для изменяемого значения
var indeskso = 0 #переменный индекс текущего языка в списке
	
func _ready():
	pass
	
func analizado_lingo(lingvoj): # создает локальный список языков (работает)
	for lingvo in lingvoj: #перебор кодов и имен языков
		loka_lingvoj[lingvo['node']['kodo']] = lingvo['node']['nomo']
		
func analizado_json(json_linio): # разбирает строку/строки из json
	if json_linio:
		loka_info = json_linio #JSON_LINIO # данные из запросы json строки с описанием параметра
		# тестирую создание Null языка при создании нового 
		print('\n--- analizado json - json linio =', json_linio) 
	else:
		print('\n--- analizado json - loka lingvoj =', loka_lingvoj) # проверяю что есть список языков
		loka_info =  {"enhavo": [''], "lingvo": {DEFA_LINGO:0}, "chefa_varianto": DEFA_LINGO} # язык ставиться по умолчанию (было null)
	chefa_varianto = loka_info['chefa_varianto'] # присваиваю значение Главного языка текущему (при открытии он выбирается по умолчанию)
	if nuna_lingvo == '' and chefa_varianto != null:  # 09/06 добавил проверку на добавление null языка, если никакой язык не выбран устанавливается язык по умолчанию
		nuna_lingvo = chefa_varianto
	$HBoxContainer/CheckBox.button_pressed = true
	$HBoxContainer/input_txt.connect("text_submitted", Callable(self, "text_submitted")) # подключается сигнал при вводе текста
	update_text() 
	add_items()
	listo_de_kodoj = loka_lingvoj.keys()
	$HBoxContainer/input_txt.grab_focus()
	
func update_text(): #обновление текстового отображения параметра
	if nuna_lingvo in loka_info['lingvo']:
		$HBoxContainer/input_txt.text = loka_info['enhavo'][loka_info['lingvo'][nuna_lingvo]]# Имя параметра из json строки (локальной переменной) #работает #data[current_lang]
	else:
		print('update nuna_lingvo = ', nuna_lingvo)
		print('loka_info lingvo = ', loka_info['lingvo'])
		$HBoxContainer/input_txt.text = '' #если нет значения строка текста пустая
	
func add_items(): #выбор языков из выпадающего списка # не выбирает текущий язык (идет по порядку)
	for key in loka_lingvoj:
		$HBoxContainer/Drop_down.add_item(loka_lingvoj[key]) # цикл добавляте языки в выпадающий список из локального словаря языков
		listo_de_lingvoj.append(loka_lingvoj[key])
		
	if nuna_lingvo:
		indeskso = listo_de_lingvoj.find(loka_lingvoj[nuna_lingvo],0) #
		$HBoxContainer/Drop_down.selected = indeskso#loka_lingvoj[nuna_lingvo]
		$HBoxContainer/Drop_down.text = loka_lingvoj[nuna_lingvo] # возможно дублирует update_text
	else:
		indeskso = 0
		$HBoxContainer/Drop_down.selected = indeskso#loka_lingvoj[nuna_lingvo]
		
func text_submitted(text): # считывание значения из текстового поля
	var loka_indekso
	varia_valoro = $HBoxContainer/input_txt.text #changed_value
	if $HBoxContainer/input_txt.has_focus() == false or Input.is_action_pressed("ui_accept"):#  проверяю фокус, если потерян сохраняю значение
		if nuna_lingvo in loka_info['lingvo']: # если текущий язык есть в словаре с индексами языков
			loka_indekso = loka_info['lingvo'][nuna_lingvo] #получаем индекс текущего языка
			loka_info['enhavo'][loka_indekso] = varia_valoro #записываем введенное значение по индексу языка 
		else: 
			loka_info['enhavo'].append(varia_valoro) #добавляю значение (имя) параметра в список имен, в конец.
	#		print('indekso varia_valoro in loka_info = ',loka_info['enhavo'].find(varia_valoro, 0), 'varia_valoro = ',varia_valoro) 
			loka_info['lingvo'][nuna_lingvo] = loka_info['enhavo'].find(varia_valoro, 0) # добавляю пару из кода языка и его индекса в списке текстовых значений параметров
		update_text() #обновляю отображение текста 

func _on_Drop_down_item_selected(index): # при выборе элемента
#	
	$HBoxContainer/Drop_down.grab_focus()
	nuna_lingvo = listo_de_kodoj[index] #присваиваем текущему язку код выбраного языка
	if chefa_varianto != '': #chefa_varianto
		if nuna_lingvo != chefa_varianto: #если текущий язык не равен главному выбору
			$HBoxContainer/CheckBox.button_pressed = false # не нажат
		elif nuna_lingvo == chefa_varianto:
			$HBoxContainer/CheckBox.button_pressed = true
	else:
		$HBoxContainer/CheckBox.disabled = false # не доступен для выбора
		$HBoxContainer/CheckBox.button_pressed = false # не нажат
	
	update_text()
	save_dafault_lang()
	print('Вывод информации: ', loka_info)

func _on_CheckBox_button_up():
	$HBoxContainer/CheckBox.grab_focus()
	if $HBoxContainer/CheckBox.pressed:
		chefa_varianto = nuna_lingvo # default_lang
	else:
		chefa_varianto = '' #default_lang
	save_dafault_lang()
	
func save_dafault_lang(): #сохранение языка по умолчанию
	loka_info['chefa_varianto'] = chefa_varianto

func _on_input_txt_focus_exited():
	text_submitted($HBoxContainer/input_txt.text)
	
func aldonu_helpilo(): #  добавить подсказку с текстом
	$Helpilo/teksto.text = $HBoxContainer/input_txt.text
	if $Helpilo/teksto.get_text().length() > $HBoxContainer/input_txt.get_size().x / 8 - 8: # проверка на пустоту строки и длину отображаемого текста # $HBoxContainer/input_txt.text != '':
		$Helpilo.set_position(Vector2($HBoxContainer/input_txt.get_position().x + 10,$HBoxContainer/input_txt.get_position().y + 60))
		$Helpilo/teksto.set_size(Vector2($HBoxContainer/input_txt.get_size().x, $Helpilo/teksto.get_size().y))
		$Helpilo/teksto.text = $HBoxContainer/input_txt.text
		prokrasto_tempo(0.4)

func prokrasto_tempo(time): # задержка
	await get_tree().create_timer(time).timeout # создание таймера на лету
	$Helpilo.show()

func _on_input_txt_mouse_entered(): # при наведении мышки отображается подсказка с полным текстом
	aldonu_helpilo()

func _on_input_txt_mouse_exited():
	$Helpilo.hide()

func _savi_datumoj(): #возвращаем обработаную строку (в виде словаря)
	# нет никаких значений
	if not len(loka_info['enhavo']):
		print('\n--- не сохраняю!!! --- ') # сообщение если нет сохранения данных
		return null
	if not loka_info['chefa_varianto']:
#		loka_info['chefa_varianto'] = 
		pass
	# проверка enhavo на предмет пустых строк и строк с пробелами
	del_empty_data()
	
	print('=== Текущая строка:  ', loka_info)
	return loka_info


func del_empty_data(): # удаление данных в json при вводе строки без значения
	for i in range(len(loka_info['enhavo']) - 1,-1,-1): #проход циклом 
		print(i)
		loka_info['enhavo'][i] = loka_info['enhavo'][i].strip_edges() #strip
		print(loka_info['enhavo'][i])
		if not loka_info['enhavo'][i]: # войдет если строка пустая
			loka_info['enhavo'].remove(i) # удаление языка по индексу из enhavo 
			korekto_ligvoi_indekso(i)
			agordi_chefa_varianto()
		
		
func korekto_ligvoi_indekso(indekso): #исправление индексов в lingvo после удаления языка
	var val
	for key in loka_info['lingvo'].keys(): #перебор словаря по ключам и значениям
		val = loka_info['lingvo'][key]
		if loka_info['lingvo'][key] > indekso:
			loka_info['lingvo'][key] -= 1
		elif loka_info['lingvo'][key] == indekso:
			loka_info['lingvo'].erase(key) # удаление языка и индекса из lingvo
	
	
func agordi_chefa_varianto(): #установка языка по умолчанию (по умолчанию устанавливается первый язык из списка)
	if chefa_varianto in loka_info['lingvo']: # проверяем есть ли язык по умолчанию в списке языков 'lingvo'
		return
	else: 
		var keys = loka_info['lingvo'].keys() #массив ключей
		var values = loka_info['lingvo'].values() #массив значений
		chefa_varianto = keys[values.find(0)] #главный язык - назначаем первый язык в списке
		loka_info['chefa_varianto'] = chefa_varianto #возможно нужно вынести в функцию сохранения
		
