extends "res://kerno/fenestroj/tipo_d1.gd"


const QueryObject = preload("queries.gd")


@onready var tree = get_node("VBox/body_texture/HSplit/Tree")


var icon_kategorio = preload("res://kerno/menuo/resursoj/icons/lefticon5.png")
var id_mendi_informoj = 0 # id запроса на корневые категории ресурсов


func _ready():
	var err = Net.connect("input_data", Callable(self, "_on_data"))
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		if int(on_data['id']) == id_mendi_informoj:
			plenigi_tree(on_data['payload']['data']['superaResursoKategorio']['edges'])
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			id_mendi_informoj = 0
			if on_data['payload']['data']['superaResursoKategorio']['pageInfo']['hasNextPage']:
				mendi_informoj(on_data['payload']['data']['superaResursoKategorio']['pageInfo']['endCursor'])
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


# запросить данные с сервера
# заказать - mendi
# сведения - informoj
func mendi_informoj(after=""):
	if !after:
		clear_listo()
	var query = QueryObject.new()
	id_mendi_informoj = Net.get_current_query_id()
	Net.send_json(query.mendo_resurso_kategorio(id_mendi_informoj, after))


# очищаем окно 
func clear_listo():
	tree.clear()


# plenigi - заполнить дерево списком категорий ресурсов, полученных с сервера
func plenigi_tree(on_data):
	if !tree.get_root():
		tree.set_hide_root(true)
		tree.create_item()
	aldoni_tree_fadeno(on_data, tree)


# fadeno - ветка (инф. (серия сообщений и комментариев по какой-л. теме форума))
func aldoni_tree_fadeno_ligilo(masivo, fadeno):
	var item
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in masivo:
		if fadeno == tree:
			item = tree.create_item()
		else:
			item = tree.create_item(fadeno)
		# сохраняес данные строки, что бы точно идентифицировать
		item.set_metadata(0,enteno.duplicate(true))
		item.set_text(0,String(enteno['node']['ligilo']['nomo']['enhavo']))
		if enteno['node']['ligilo'].get('ligilo') and\
				len(enteno['node']['ligilo']['ligilo']['edges'])>0:
			aldoni_tree_fadeno_ligilo(enteno['node']['ligilo']['ligilo']['edges'], item)


# fadeno - ветка (инф. (серия сообщений и комментариев по какой-л. теме форума))
func aldoni_tree_fadeno(masivo, fadeno):
	var item
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in masivo:
		if fadeno == tree:
			item = tree.create_item()
		else:
			item = tree.create_item(fadeno)
		# сохраняес данные строки, что бы точно идентифицировать
		item.set_metadata(0,enteno.duplicate(true))
		item.set_text(0,String(enteno['node']['nomo']['enhavo']))
		item.set_icon(0,icon_kategorio)
		if len(enteno['node']['ligilo']['edges'])>0:
			aldoni_tree_fadeno_ligilo(enteno['node']['ligilo']['edges'], item)


func _on_Tree_focus_entered():
	fenestro_supren()


func _on_Tree_cell_selected():
	var select = tree.get_next_selected(null)
	# если есть вложенные данные - выходим
	if not select.get_children():
		return null
	var resurso = select.get_metadata(0)
	if !resurso:
		return null
	
	# отправляем запрос на получение списка вложенных данных, если их нет
