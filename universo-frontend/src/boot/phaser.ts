import { boot } from 'quasar/wrappers';
import { App } from 'vue';
import * as Phaser from 'phaser';
// import RexUIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin.js';

export default boot(({ app }: { app: App }) => {
  app.config.globalProperties.$phaser = Phaser;
  // app.config.globalProperties.$rexUI = RexUIPlugin;
});

export { Phaser };
