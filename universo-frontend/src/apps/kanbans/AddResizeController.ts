const AddResizeController = (sizer, board): void => {
  const scene = sizer.scene;

  const offsetX = 20;
  const offsetY = 20;

  const bottomRighterController = scene.add.image(
    sizer.right - offsetX,
    sizer.bottom - offsetY,
    'icon',
  );

  let dragOffsetX = 0;
  let dragOffsetY = 0;

  bottomRighterController.setInteractive({ draggable: true });

  bottomRighterController.on('dragstart', (pointer: Phaser.Input.Pointer) => {
    dragOffsetX = pointer.x - bottomRighterController.x;
    dragOffsetY = pointer.y - bottomRighterController.y;
  });
  bottomRighterController.on('dragend', (pointer: Phaser.Input.Pointer) => {
    const { x, y, height, width } = sizer;

    scene.store.onEditKanvasoObjekto({
      koordinatoX: x,
      koordinatoY: y,
      longo: height,
      largxo: width,
      uuid: board.uuid,
    });
  });

  bottomRighterController.on(
    'drag',
    (pointer: Phaser.Input.Pointer, dragX, dragY) => {
      const topX = sizer.left;
      const topY = sizer.top;
      const width = dragX + dragOffsetX - topX;
      const height = dragY + dragOffsetY - topY;

      sizer.setMinSize(width, height).layout();
    },
  );

  // Обновляем позицию AddResizeController каждый раз, когда происходит обновление сцены
  scene.events.on('update', () => {
    bottomRighterController.setPosition(
      sizer.right - offsetX,
      sizer.bottom - offsetY,
    );
  });

  sizer.pin(bottomRighterController);
};

export default AddResizeController;
