import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanelsBox from './CreateColumnPanelsBox';
import CreateHeader from './CreateHeader';
import AddResizeController from './AddResizeController';
import ObjectWrapper from 'src/utils/objectWrapper';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { EventBus, Events } from 'src/boot/eventBus';
import { getAllChildrens } from '../utils/GetAllChildrens';

const boardsBoxMap = new Map();

const CreateScrollablePanel = (scene: EndlessCanvas, board: ObjectWrapper) => {
  const config = {
    adaptThumbSizeMode: true,
    width: board.sizes.width,
    height: board.sizes.height,
    //@ts-ignore
    background: scene.rexUI.add.roundRectangle({
      radius: 10,
      strokeColor: COLOR_DARK,
    }),
    panel: {
      child: CreateColumnPanelsBox(scene, board),
      mask: {
        padding: 2,
        updateMode: 'everyTick',
      },
    },

    sliderX: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    sliderY: {
      track: { width: 20, radius: 10, color: COLOR_DARK },
      thumb: { radius: 13, color: COLOR_LIGHT },
      hideUnscrollableSlider: true,
      input: 'click',
    },
    scrollerX: false,
    scrollerY: false,
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
      sliderX: 10,
      sliderY: 10,
    },
    expand: {
      header: true,
      footer: true,
      panel: true,
    },
    header: CreateHeader(scene, board),
  };

  // Создаем ScrollablePanel
  //@ts-ignore
  const scrollablePanel = scene.rexUI.add.scrollablePanel(config);

  // Устанавливаем возможность перетаскивания
  scrollablePanel.setDraggable('header');
  scrollablePanel.on('sizer.dragend', () => {
    const { x, y, height, width } = scrollablePanel;
    scene.store.onEditKanvasoObjekto({
      koordinatoX: x,
      koordinatoY: y,
      longo: height,
      largxo: width,
      uuid: board.uuid,
    });
  });

  EventBus.$on(Events.Change, (uuid, object) => {
    if (uuid === board.uuid) {
      scrollablePanel.setMinSize(object.sizes.width, object.sizes.height);
      scrollablePanel.setPosition(object.sizes.x, object.sizes.y);
      scrollablePanel.layout();
    }
  });

  // Вызываем метод layout для обновления компоновки
  scrollablePanel.layout();

  // Добавляем контроллер для изменения размера
  AddResizeController(scrollablePanel, board);

  EventBus.$on(Events.Delete, (uuid, object) => {
    if (boardsBoxMap.has(uuid)) {
      boardsBoxMap.get(uuid).clear(true);
      boardsBoxMap.get(uuid).destroy();
      boardsBoxMap.delete(uuid);
    }
  });
  scrollablePanel.setOrigin(0, 0);
  scrollablePanel.setPosition(board.sizes.x, board.sizes.y);
  scrollablePanel.layout();

  return scrollablePanel;
};

export default (scene: EndlessCanvas, boards: ObjectWrapper[]) => {
  EventBus.$on(Events.Create, (uuid, board) => {
    if (!board.hasParent) {
      const scrollablePanel = CreateScrollablePanel(scene, board);
      boardsBoxMap.set(board.uuid, scrollablePanel);
      scene.cameras.getCamera('ui').ignore(getAllChildrens(scrollablePanel));
      scene.cameras.getCamera('ui').ignore(scrollablePanel);
    }
  });

  return boards.map((board: ObjectWrapper) => {
    const scrollablePanel = CreateScrollablePanel(scene, board);
    boardsBoxMap.set(board.uuid, scrollablePanel);
    return scrollablePanel;
  });
};
