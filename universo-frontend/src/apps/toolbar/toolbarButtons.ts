import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import createBoardCallback from './createBoardCallback';

export const toolbarButtons = (scene: EndlessCanvas) => [
  // {
  //   image: scene.isDark ? 'toolbar_1b' : 'toolbar_1c',
  //   imageHover: 'toolbar_1a',
  //   callback: function () {
  //     // scene.router.push('/projects');
  //   }
  // },
  // {
  //   image: scene.isDark ? 'toolbar_2b' : 'toolbar_2c',
  //   imageHover: 'toolbar_2a',
  //   callback: function () {
  //     // scene.router.push('/projects');
  //   }
  // },
  {
    image: scene.isDark ? 'toolbar_3b' : 'toolbar_3c',
    imageHover: 'toolbar_3a',
    callback: () => createBoardCallback(scene),
  },
  // {
  //   image: scene.isDark ? 'toolbar_4b' : 'toolbar_4c',
  //   imageHover: 'toolbar_4a',
  //   callback: function () {
  //     // scene.router.push('/projects');
  //   }
  // },
];
