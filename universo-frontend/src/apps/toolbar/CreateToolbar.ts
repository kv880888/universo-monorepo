import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';

function CreateToolbar(scene: EndlessCanvas, buttonArray) {
  const toolbarWidth = scene.toolbarWidth; // Ширина главного меню
  const arr = [];
  buttonArray.forEach((element, index) => {
    const button = scene.add.sprite(0, 0, element.image).setDepth(1010);
    button.setScale(toolbarWidth / button.height);
    const groupHeight = (button.displayHeight + 5) * buttonArray.length;
    const screenHeight = parseInt(scene.sys.game.config.height as string, 10); // Высота экрана

    button.setPosition(
      button.displayWidth / 2 + 10,
      screenHeight / 2 -
        groupHeight / 2 +
        (button.displayHeight + 5) / 2 +
        (button.displayHeight + 5) * index,
    );
    button.setInteractive({ cursor: 'pointer' });

    button.on('pointerdown', element.callback);
    const fx = button.preFX.addGlow();
    fx.active = false;
    button
      .on('pointerover', (button1, groupName, index, pointer, event) => {
        button.setTexture(element.imageHover);
        fx.active = true;
      })
      .on('pointerout', (button1, groupName, index, pointer, event) => {
        button.setTexture(element.image);
        fx.active = false;
      });
    arr.push(button);
  });

  return arr;
}
export default CreateToolbar;
