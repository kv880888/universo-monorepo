import ObjectWrapper from 'src/utils/objectWrapper';
import {
  COLOR_PRIMARY,
  DefaultDepth,
  DragObjectDepth,
  FONT_SMALL,
  COLOR_DRAG_STROKE,
} from '../kanbans/Const';
import { EventBus, Events } from 'src/boot/eventBus';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import CreateDropDownList from '../kanbans/CreateDropDownList';
import { createModal } from '../kanbans/CreateModal';

const CreateItem = (scene: EndlessCanvas, card: ObjectWrapper) => {
  let textInput = '';
  const sizer = scene.rexUI.add
    .sizer({
      orientation: 'x',
      space: {
        left: 5,
        right: 0,
        top: 5,
        bottom: 5,
      },
    })
    .addBackground(
      //@ts-ignore
      scene.rexUI.add.roundRectangle({
        radius: 10,
        color: COLOR_PRIMARY,
      }),
      //@ts-ignore
      'background',
    );
  const innerText = scene.rexUI.add.label({
    text: scene.add.text(0, 0, card.name || '', {
      fontSize: FONT_SMALL,
      wordWrap: { width: 160 },
    }),
  });

  const dropDownOptions = [
    {
      label: 'Редактировать карточку',
      id: 0,
      onClick: () => {
        createModal(
          scene,
          (text) => {
            textInput = text;
          }, //@ts-ignore
          scene.store.getCurrentState.get(card.uuid).name,
        ).then((data) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (data.text === 'Сохранить' && textInput) {
            const payload = {
              nomo: textInput,
              uuid: card.uuid,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
    {
      label: 'Удалить карточку',
      id: 1,
      onClick: () => {
        createModal(
          scene,
          null,
          null,
          'Удалить карточку',
          'Отменить',
          'Удалить',
        ).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Удалить') {
            const payload = {
              uuid: card.uuid,
              forigo: true,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
  ];

  const dropDownButton = CreateDropDownList(scene, dropDownOptions);

  sizer
    .add(innerText, { proportion: 1, expand: true })
    .add(dropDownButton, { proportion: 0, expand: true });

  SetDraggable(sizer);

  EventBus.$on(Events.Change, (uuid, object) => {
    if (uuid === card.uuid) {
      //@ts-ignore
      innerText.setText(scene.store.getCurrentState.get(uuid)?.name);
      sizer.getTopmostSizer().layout();
    }
  });
  sizer.layout();

  return sizer;
};

const SetDraggable = (item: any): void => {
  item
    .setDraggable({
      sensor: item,
      target: item,
    })
    .on('sizer.dragstart', () => OnItemDragStart.call(item))
    .on('sizer.dragend', () => OnItemDragEnd.call(item));
};

const OnItemDragStart = function (this): void {
  this.setDepth(DragObjectDepth);
  this.getElement('background').setStrokeStyle(3, COLOR_DRAG_STROKE);
};

const OnItemDragEnd = function (this): void {
  this.setDepth(DefaultDepth);
  this.getElement('background').setStrokeStyle();
};

export default CreateItem;
