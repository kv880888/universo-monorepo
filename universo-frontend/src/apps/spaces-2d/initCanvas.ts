import CreateMoreScrollablePanels from '../kanbans/CreateScrollablePanel';

export default (scene) => {
  CreateMoreScrollablePanels(scene, scene.store.getTree);
};
