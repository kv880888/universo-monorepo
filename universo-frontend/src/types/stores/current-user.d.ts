export interface UserResponse {
  objId: number;
  unuaNomo: UnuaNomo;
  universoUzanto: UniversoUzanto;
  duaNomo: DuaNomo;
  familinomo: Familinomo;
  chefaRetposhto: string;
  avataro: Avataro;
}

export interface CurrentUserState {
  user: Mi | null;
  jwt: string | null;
  isLoggedIn: boolean | null;
  konfirmita: boolean | null;
  isAdmin: boolean | null;
  authtoken: string | null;
  csrftoken: string | null;
  organizations: null | { edges: [{ node: { uuid: number } }] };
  authData: object | null;
  counter: number;
  getJWTpayload?: GetJwtPayload;
  ws_connected: Boolean;
}
export interface Getters {
  getUserId: number;
  getJWTpayload: string[];
}

export interface UnuaNomo {
  enhavo: string;
}

export interface DuaNomo {
  enhavo: string;
}

export interface Familinomo {
  enhavo: string;
}

export interface ChefaLingvo {
  id: string;
  nomo: string;
}

export interface Nomo {
  lingvo: string;
  enhavo: string;
  chefaVarianto: boolean;
}

export interface Loghlando {
  nomo: Nomo;
  id: string;
}

export interface Loghregiono {
  id: string;
  nomo: Nomo;
}

export interface BildoE {
  url: string;
}

export interface BildoF {
  url: string;
}

export interface Avataro {
  id: string;
  bildoE: BildoE;
  bildoF: BildoF;
}

export interface Statistiko {
  miaGekamarado: boolean;
  miaGekamaradoPeto: boolean;
  kandidatoGekamarado: boolean;
  tutaGekamaradoj: number;
  rating?: any;
  aktivaDato?: any;
}

export interface ChefaOrganizo {
  id: string;
  nomo: Nomo;
}

export interface Node {
  id: string;
  nomo: Nomo;
}

export interface Sovetoj {
  edges: Edge[];
}

export interface Edge {
  node: Node;
}

export interface Sindikatoj {
  edges: Edge[];
}

export interface AdministritajKomunumoj {
  edges: any[];
}

export interface UniversoUzanto {
  id: string;
  uuid: string;
  retnomo: string;
}

export interface Mi {
  id: string;
  objId: number;
  uuid: string;
  unuaNomo: UnuaNomo;
  duaNomo: DuaNomo;
  familinomo: Familinomo;
  sekso: string;
  konfirmita: boolean;
  isActive: boolean;
  chefaLingvo: ChefaLingvo;
  chefaTelefonanumero: string;
  chefaRetposhto: string;
  naskighdato: string;
  loghlando: Loghlando;
  loghregiono: Loghregiono;
  agordoj: string;
  avataro: Avataro;
  statuso?: any;
  statistiko: Statistiko;
  kontaktaInformo?: any;
  kandidatojTuta: number;
  gekamaradojTuta: number;
  chefaOrganizo: ChefaOrganizo;
  sovetoj: Sovetoj;
  sindikatoj: Sindikatoj;
  administritajKomunumoj: AdministritajKomunumoj;
  universoUzanto: UniversoUzanto;
  isAdmin: boolean;
}

export interface MiWrapper {
  mi: Mi;
}
