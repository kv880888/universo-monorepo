import { apollo } from 'src/boot/apollo';
import { debugLog } from 'src/utils';

/**
 * Функция для выполнения GraphQL запроса.
 * @param query - Запрос GraphQL.
 * @param variables - Переменные запроса.
 * @param message - Сообщение для отладки.
 * @returns Ответ от сервера.
 */
export async function executeQuery(
  query: any,
  variables: any,
  message: string,
): Promise<any> {
  debugLog(message);
  try {
    const response = await apollo.default.query({
      query,
      variables,
      errorPolicy: 'all',
      fetchPolicy: 'network-only',
    });
    return response;
  } catch (err) {
    debugLog('GraphQL Query Error:', err);
    throw err;
  }
}

/**
 * Функция для выполнения GraphQL мутации.
 * @param mutation - Мутация GraphQL.
 * @param variables - Переменные мутации.
 * @param message - Сообщение для отладки.
 * @returns Ответ от сервера.
 */
export async function executeMutation(
  mutation: any,
  variables: any,
  message: string,
): Promise<any> {
  debugLog(message);
  try {
    const response = await apollo.default.mutate({
      mutation,
      variables,
      errorPolicy: 'all',
      fetchPolicy: 'network-only',
    });
    return response;
  } catch (err) {
    debugLog('GraphQL Mutation Error:', err);
    throw err;
  }
}
