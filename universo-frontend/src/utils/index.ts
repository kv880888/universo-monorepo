/**
 * Выводит лог в зависимости от установки переменной окружения DEBUG
 *
 * @export
 * @param {string} item строка для вывода в лог
 */
export function debugLog(...items) {
  if (process.env.DEBUG === 'true') {
    console.log(...items);
  }
}

/** Возвращает url бакенда в зависимости от контекста
 * @returns {string}
 */
export function getAPIurl() {
  return process.env.MODE === 'capacitor'
    ? (
        process.env.CAPACITOR_GRAPHQL_URI ||
        process.env.GRAPHQL_URI ||
        'http://localhost:8000/api/v1.1/'
      ).replace(/['"]/g, '')
    : (process.env.GRAPHQL_URI || 'http://localhost:8000/api/v1.1/').replace(
        /['"]/g,
        '',
      );
}

/**
 * Возвращает копию переданного в аргументе
 *
 * @export
 * @param {*} obj
 * @return {*}
 */
export function copy(obj) {
  if (typeof obj !== 'object' || obj === null) {
    return obj; // Примитивы возвращаются как есть
  }

  if (Array.isArray(obj)) {
    return obj.map((item) => copy(item)); // Копирование массива
  }

  if (obj instanceof Map) {
    return new Map([...obj.entries()].map(([key, val]) => [key, copy(val)])); // Копирование Map
  }

  if (obj instanceof Set) {
    return new Set([...obj].map((item) => copy(item))); // Копирование Set
  }

  if (obj instanceof Function) {
    const cloneFunction = obj.bind(this); // Копирование функции
    Object.assign(cloneFunction, obj);
    return cloneFunction;
  }

  return Object.assign(
    {},
    obj,
    Object.keys(obj).reduce((acc, key) => {
      acc[key] = copy(obj[key]); // Копирование свойств объекта
      return acc;
    }, {}),
  );
}
